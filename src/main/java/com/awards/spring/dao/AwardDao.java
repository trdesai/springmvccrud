package com.awards.spring.dao;

import java.util.List;

import com.awards.spring.model.Award;

public interface AwardDao {

   long save(Award book);

   Award get(long id);

   List<Award> list();

   void update(long id, Award book);

   void delete(long id);

}
