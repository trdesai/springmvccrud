package com.awards.spring.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.awards.spring.model.Award;

@Repository
public class AwardDaoImp implements AwardDao {

   @Autowired
   private SessionFactory sessionFactory;

   @Override
   public long save(Award book) {
      sessionFactory.getCurrentSession().save(book);
      return book.getId();
   }

   @Override
   public Award get(long id) {
      return sessionFactory.getCurrentSession().get(Award.class, id);
   }

   @Override
   public List<Award> list() {
      Session session = sessionFactory.getCurrentSession();
      CriteriaBuilder cb = session.getCriteriaBuilder();
      CriteriaQuery<Award> cq = cb.createQuery(Award.class);
      Root<Award> root = cq.from(Award.class);
      cq.select(root);
      Query<Award> query = session.createQuery(cq);
      return query.getResultList();
   }

   @Override
   public void update(long id, Award award) {
      Session session = sessionFactory.getCurrentSession();
      Award award2 = session.byId(Award.class).load(id);
      award2.setTitle(award.getTitle());
      award2.setAwardee(award.getAwardee());
      award2.setDonor(award.getDonor());
      session.flush();
   }

   @Override
   public void delete(long id) {
      Session session = sessionFactory.getCurrentSession();
      Award book = session.byId(Award.class).load(id);
      session.delete(book);
   }

}
