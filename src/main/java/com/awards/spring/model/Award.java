package com.awards.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "Award")
public class Award {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   private String title;
   private String awardee;
   private String donor;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getAwardee() {
      return awardee;
   }

   public void setAwardee(String awardee) {
      this.awardee = awardee;
   }
   
   public String getDonor() {
	      return donor;
	   }

   public void setDonor(String donor) {
	  this.donor = donor;
	   }

}
