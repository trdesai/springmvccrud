package com.awards.spring.service;

import java.util.List;

import com.awards.spring.model.Award;

public interface AwardService {

   long save(Award book);
   Award get(long id);
   List<Award> list();
   void update(long id, Award award);
   void delete(long id);
}
