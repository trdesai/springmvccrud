package com.awards.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.awards.spring.dao.AwardDao;
import com.awards.spring.model.Award;

@Service
@Transactional(readOnly = true)
public class AwardServiceImp implements AwardService {

   @Autowired
   private AwardDao awardDao;

   @Transactional
   @Override
   public long save(Award award) {
      return awardDao.save(award);
   }

   @Override
   public Award get(long id) {
      return awardDao.get(id);
   }

   @Override
   public List<Award> list() {
      return awardDao.list();
   }

   @Transactional
   @Override
   public void update(long id, Award award) {
	   awardDao.update(id, award);
   }

   @Transactional
   @Override
   public void delete(long id) {
	   awardDao.delete(id);
   }

}
