package com.awards.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.awards.spring.model.Award;
import com.awards.spring.service.AwardService;

@RestController
public class AwardController {

   @Autowired
   private AwardService awardService;

   /*---Add new award---*/
   @PostMapping("/nominate")
   public ResponseEntity<?> save(@RequestBody Award award) {
      long id = awardService.save(award);
      return ResponseEntity.ok().body("Award has been created with ID:" + id);
   }

   /*---Get a award by id---*/
   @GetMapping("/award/{id}")
   public ResponseEntity<Award> get(@PathVariable("id") long id) {
      Award award = awardService.get(id);
      return ResponseEntity.ok().body(award);
   }

   /*---get all awards---*/
   @GetMapping("/awards")
   public ResponseEntity<List<Award>> list() {
      List<Award> awards = awardService.list();
      return ResponseEntity.ok().body(awards);
   }

   /*---Update a award by id---*/
   @PutMapping("/award/{id}")
   public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Award award) {
	   awardService.update(id, award);
      return ResponseEntity.ok().body("Award has been updated successfully.");
   }

   /*---Delete a award by id---*/
   @DeleteMapping("/award/{id}")
   public ResponseEntity<?> delete(@PathVariable("id") long id) {
	   awardService.delete(id);
      return ResponseEntity.ok().body("Award has been deleted successfully.");
   }
}